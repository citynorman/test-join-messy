# -*- coding: utf-8 -*-
"""
Created on Fri Jan 12 10:46:13 2018

@author: niemern
"""

import jellyfish
import metrics
from importlib import reload
import ngram
reload(ngram)

t1="100"
t21=["100a","100b","101","110","a100","b100"]#,"aa100"] # case 1. expecting: similar, not similar
t22=["101","105","200"] # case 2. expecting: similar, less similar, least similar

fun = jellyfish.levenshtein_distance
print([fun(t1, t) for t in t21])
# print([fun(t1, t) for t in t22])

reload(metrics)
fun = metrics.levenshtein_distance
print([fun(t1, t) for t in t21])
# print([fun(t1, t) for t in t22])

fun = jellyfish.levenshtein_distance
print([fun(t1, t) for t in t21])
print([fun(t1, t) for t in t22])

fun = metrics._jaro_winkler
print([fun(t1, t) for t in t21])
print([fun(t1, t) for t in t22])

ngram.CharNgrams("aaa") * ngram.CharNgrams("aaaab")
ngram.CharNgrams("aaa") * ngram.CharNgrams("baaaa")
ngram.Ngrams("100") * ngram.Ngrams("a100asdfaesf")

import uuid
from itertools import takewhile
strings=['pre-'+str(uuid.uuid4())+'-post' for _ in range(int(1e4))]
strings1=[s[::-1] for s in strings]

''.join(c[0] for c in takewhile(lambda x: all(x[0] == y for y in x), zip(*strings)))
''.join(c[0] for c in takewhile(lambda x: all(x[0] == y for y in x), zip(*strings1)))[::-1]


def all_same(x):
    return all(x[0] == y for y in x)

char_tuples = zip(*strings1)
prefix_tuples = takewhile(all_same, char_tuples)
''.join(x[0] for x in prefix_tuples)

import timeit
timeit.timeit("''.join(c[0] for c in takewhile(lambda x: all(x[0] == y for y in x), zip(*strings)))", number=10000)
