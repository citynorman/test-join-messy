# -*- coding: utf-8 -*-
"""
Created on Wed Nov 29 13:37:41 2017

@author: deepmind
"""
import pandas as pd
import dask.dataframe as dd

#***************************************************************
# sec master
#***************************************************************
df_sm2 = pd.read_csv('secmaster-msci-us.csv', dtype='str')
#df_sm2['date']=pd.to_datetime(df_sm2['date'],format='%Y-%m-%d')
idxSelDate = df_sm2['date'].isin(['2017-03-31', '2017-04-28'])
df_sm2 = df_sm2[idxSelDate]
df_sm2.to_csv('test-merge-securities.csv',index=False)

df_sm2.info()

#***************************************************************
# factors
#***************************************************************
ddf_factors = dd.read_csv('rank_AFLUS_*.txt', dtype='str', sep='\t')
df_factors = ddf_factors.compute()
#df_factors['Date']=pd.to_datetime(df_factors['Date'],format='%Y-%m-%d')

cfg_col_sel = ['Date', 'cusip', 'sedol', 'isin', 'ticker', 'compustat_id', 'ROIC','ROE', 'ROA', 'CFROIC']
df_factors = df_factors[cfg_col_sel]

df_factors.info()
df_factors.to_csv('test-merge-factors.csv',index=False)

