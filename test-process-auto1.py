# -*- coding: utf-8 -*-
"""
Created on Tue Jan  9 13:22:09 2018

@author: niemern
"""
import pandas as pd
import numpy as np

#***************************************************************
# join attempt
#***************************************************************
df_sec = pd.read_csv('test-merge-securities.csv', dtype='str')
df_fact = pd.read_csv('test-merge-factors.csv', dtype='str')

# merge manual
df_sec['BARRA_PIT_CUSIP2']=df_sec['BARRA_PIT_CUSIP'].str[2:-1] 
df_fact['cusip2']=df_fact['cusip'].str[:-1] 
df_merge = df_fact.merge(df_sec, left_on=['cusip2','Date'], right_on=['BARRA_PIT_CUSIP2','date'])
df_merge.shape
df_merge = df_merge.sort_values('BARRA_PIT_CUSIP')

# get ids
id1in = np.sort(df_merge['BARRA_PIT_CUSIP'].unique())
id2in = np.sort(df_merge['cusip'].unique())
id2inraw = np.sort(df_fact['cusip'].unique().tolist())
id1out = np.sort(df_merge['BARRA_PIT_CUSIP2'].unique())
id2out = np.sort(df_merge['cusip2'].unique())

id1in[:5]
id2in[:5]
id1out[:5]
id1out[:5]

import editdistance
import distance
import jellyfish

def match(i1,i2):
    return i1[2:-1], i2[:-1]

id11=id1in[0]
dist_id2in = [editdistance.eval(id11, i) for i in id2in]
print(id2in[np.argmin(dist_id2in)],id11)

dist_id2inraw = [editdistance.eval(id11, i) for i in id2inraw]
print(id2inraw[np.argmin(dist_id2inraw)],id11)

dist_id2inraw = [distance.lcsubstrings(id11, i) for i in id2in[10]]
print(id2inraw[np.argmin(dist_id2inraw)],id11)


distance.lcsubstrings(id11, id2in[0])

dist_id2inraw = [distance.jaccard(id11, i) for i in id2inraw]
print(id2inraw[np.argmin(dist_id2inraw)],id11)

list(distance.ifast_comp(id11, id2inraw))[:10]

dist_id2inraw = [jellyfish.jaro_winkler(id11, i) for i in id2inraw]
print(id2inraw[np.argmax(dist_id2inraw)],id11)

jellyfish.levenshtein_distance("preabc-001055102","b'00105510'")


import hyperloglog
hll = hyperloglog.HyperLogLog(0.01)
hll.add(df_merge['BARRA_PIT_CUSIP'].unique().tolist())
