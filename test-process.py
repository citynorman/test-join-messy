# -*- coding: utf-8 -*-
"""
Created on Wed Nov 29 13:13:27 2017

@author: deepmind
"""
import pandas as pd
import numpy as np

#***************************************************************
# join attempt
#***************************************************************
df_sec = pd.read_csv('test-merge-securities.csv', dtype='str')
df_fact = pd.read_csv('test-merge-factors.csv', dtype='str')

# try merging
df_merge = df_fact.merge(df_sec, left_on=['cusip','Date'], right_on=['BARRA_PIT_CUSIP','date'])
print(df_merge.shape) # didnt' work!

# remove weird characters in identifier
df_sec['BARRA_PIT_CUSIP2']=df_sec['BARRA_PIT_CUSIP'].str[2:-1] 

# try merging
df_merge = df_fact.merge(df_sec, left_on=['cusip','Date'], right_on=['BARRA_PIT_CUSIP2','date'])
print(df_merge.shape) # still didnt' work!

# check length...
len(df_sec['BARRA_PIT_CUSIP2'].values[0])
len(df_fact['cusip'].values[0])

# fix different id length issue
df_fact['cusip2']=df_fact['cusip'].str[:-1] 
len(df_fact['cusip2'].values[0])

# try merging
df_merge = df_fact.merge(df_sec, left_on=['cusip2','Date'], right_on=['BARRA_PIT_CUSIP2','date'])
print(df_merge.shape) # worked!
print(df_sec.shape) # but wait... why less than half the rows...?

# check dates
df_sec['date'].unique()
df_fact['Date'].unique()

# fix month-end issue
df_sec['date_month']=df_sec['date'].str[:7] 
df_fact['date_month']=df_fact['Date'].str[:7] 

# try merging
df_merge = df_fact.merge(df_sec, left_on=['cusip2','date_month'], right_on=['BARRA_PIT_CUSIP2','date_month'])
print(df_merge.shape) # worked!
print(df_merge['cusip2'].unique().shape)
print(df_sec['BARRA_PIT_CUSIP2'].unique().shape) # lost one company...
print(df_sec.groupby('date').size()) # lost one company...
print(df_merge.groupby('date').size()) # day 1 we lost a bunch of companies

#***************************************************************
# join coverage
#***************************************************************
df_merge_max = df_fact.merge(df_sec, left_on=['cusip2','date_month'], right_on=['BARRA_PIT_CUSIP2','date_month'], how='right')
print(df_merge.shape[0]/df_merge_max.shape[0]) # pretty good
df_merge_max.loc[df_merge_max['ROIC'].isnull(),'cusip2'].unique().shape[0] # missing a number of tickers

df_merge.info()
df_merge_max.info()

#***************************************************************
# what if sedol and cusip complement each other?
#***************************************************************
df_sec['BARRA_PIT_SEDOL2']=df_sec['BARRA_PIT_SEDOL'].str[2:-1] 
df_fact['sedol2']=df_fact['sedol'].str[:-1] 

cfg_nan_cusip = df_sec['BARRA_PIT_CUSIP2'].unique()[:25]
cfg_nan_sedol = df_sec['BARRA_PIT_SEDOL2'].unique()[-50:]

df_sec2 = df_sec.copy()
df_sec2.loc[df_sec['BARRA_PIT_CUSIP2'].isin(cfg_nan_cusip),'BARRA_PIT_CUSIP2']=np.nan
df_sec2.loc[df_sec['BARRA_PIT_SEDOL2'].isin(cfg_nan_sedol),'BARRA_PIT_SEDOL2']=np.nan

df_merge = df_fact.merge(df_sec2, left_on=['sedol2','date_month'], right_on=['BARRA_PIT_SEDOL2','date_month'])
print(df_merge.shape) # woa?! what happened...?

df_merge = df_fact.merge(df_sec2, left_on=['cusip2','date_month'], right_on=['BARRA_PIT_CUSIP2','date_month'])
print(df_merge.shape) # woa?! what happened...?

# delete nan identifiers that get merged
df_sec3 = df_sec.dropna()

df_merge = df_fact.merge(df_sec3, left_on=['sedol2','date_month'], right_on=['BARRA_PIT_SEDOL2','date_month'])
print(df_merge.shape) # back to normal
print(df_merge.groupby('date').size()) # losing lot of companies...

df_merge = df_fact.merge(df_sec3, left_on=['cusip2','date_month'], right_on=['BARRA_PIT_CUSIP2','date_month'])
print(df_merge.shape) # back to normal
print(df_merge.groupby('date').size()) # losing lot of companies...

# drop nans differently
idxSel = ~df_sec2['BARRA_PIT_CUSIP2'].isnull()
df_merge1 = df_fact.merge(df_sec2[idxSel], left_on=['cusip2','date_month'], right_on=['BARRA_PIT_CUSIP2','date_month'])
print(df_merge1.shape) # back to normal
print(df_merge1.groupby('date').size()) # losing less companies...

idxSel = ~df_sec2['BARRA_PIT_SEDOL2'].isnull()
df_merge2 = df_fact.merge(df_sec2[idxSel], left_on=['sedol2','date_month'], right_on=['BARRA_PIT_SEDOL2','date_month'])
print(df_merge2.shape) # back to normal
print(df_merge2.groupby('date').size()) # losing less companies...

matched_cusip = set(df_merge1['TQA_ID'].unique().tolist())
matched_sedol = set(df_merge2['TQA_ID'].unique().tolist())
matched_total = set(df_sec['TQA_ID'].unique().tolist())

print('matched companies:','cusip merge',len(matched_cusip), 'sedol merge',len(matched_sedol), 'combined merge',len(matched_cusip|matched_sedol))
print('coveage:','cusip merge',round(len(matched_cusip)/len(matched_total),3), 'sedol merge',round(len(matched_sedol)/len(matched_total),3), 'combined merge',round(len(matched_cusip|matched_sedol)/len(matched_total),3))
