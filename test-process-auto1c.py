# -*- coding: utf-8 -*-
"""
Created on Tue Jan  9 13:22:09 2018

@author: niemern
"""
import pandas as pd
import numpy as np
import jellyfish
import distance
import swalign

#***************************************************************
# join attempt
#***************************************************************
df_sec = pd.read_csv('test-merge-securities.csv', dtype='str')
df_fact = pd.read_csv('test-merge-factors.csv', dtype='str')

# merge manual
df_sec['BARRA_PIT_CUSIP2']=df_sec['BARRA_PIT_CUSIP'].str[2:-1] 
df_fact['cusip2']=df_fact['cusip'].str[:-1] 
df_merge = df_fact.merge(df_sec, left_on=['cusip2','Date'], right_on=['BARRA_PIT_CUSIP2','date'])
df_merge.shape
df_merge = df_merge.sort_values('BARRA_PIT_CUSIP')

pool_id1 = (df_merge['BARRA_PIT_CUSIP']).unique() # +df_merge['BARRA_PIT_SEDOL']
df_fact['tmpid']=df_fact['cusip']
df_fact2 = df_fact.dropna(subset=['cusip','sedol'])
pool_id2 = (df_merge['cusip']).unique() #+df_merge['sedol']
# choose your own values here… 2 and -1 are common.
match = 2
mismatch = -1
scoring = swalign.NucleotideScoringMatrix(match, mismatch)
sw = swalign.LocalAlignment(scoring)  # you can also choose gap penalties, etc...
id2out_calc = []
id2out_dist = []
for id11 in pool_id1:
#    dist_id2inraw = [len(distance.lcsubstrings(id11, i)) for i in pool_id2]
    dist_id2inraw = [sw.align(id11, i).score for i in pool_id2]
    id2out_calc.append(pool_id2[np.argmax(dist_id2inraw)])
    id2out_dist.append(dist_id2inraw[np.argmax(dist_id2inraw)])
    if np.sum(np.array(dist_id2inraw==np.max(dist_id2inraw)))>1:
        print('duplicate')

id11
np.sort(dist_id2inraw)[-5:]
pool_id2[np.argsort(dist_id2inraw)[-5:]]


df_chk = pd.DataFrame({'id1':pool_id1[:10],'id2':id2out_calc,'id_dist':id2out_dist})
df_chk.head()

df_chk.sort_values('id_dist').head()
df_chk['id_dist'].unique().shape

df_merge2 = df_merge.merge(df_chk, left_on='BARRA_PIT_CUSIP', right_on='id1')#, how='left')
df_merge2.shape
(df_merge2['id2']==df_merge2['cusip']).sum()/df_merge2.shape[0]
dft=df_merge2[df_merge2['id2']!=df_merge2['cusip']]

