# -*- coding: utf-8 -*-
"""
Created on Tue Jan  9 13:22:09 2018

@author: niemern
"""
import pandas as pd
import numpy as np
import jellyfish

#***************************************************************
# join attempt
#***************************************************************
df_sec = pd.read_csv('test-merge-securities.csv', dtype='str')
df_fact = pd.read_csv('test-merge-factors.csv', dtype='str')

# merge manual
df_sec['BARRA_PIT_CUSIP2']=df_sec['BARRA_PIT_CUSIP'].str[2:-1] 
df_fact['cusip2']=df_fact['cusip'].str[:-1] 
df_merge = df_fact.merge(df_sec, left_on=['cusip2','Date'], right_on=['BARRA_PIT_CUSIP2','date'])
df_merge.shape
df_merge = df_merge.sort_values('BARRA_PIT_CUSIP')

pool_id1 = (df_merge['BARRA_PIT_CUSIP']+df_merge['BARRA_PIT_SEDOL']).unique()
df_fact['tmpid']=df_fact['cusip']
df_fact2 = df_fact.dropna(subset=['cusip','sedol'])
pool_id2 = (df_merge['cusip']+df_merge['sedol']).unique()
id2out_calc = []
id2out_dist = []
for id11 in pool_id1:
    dist_id2inraw = [jellyfish.jaro_winkler(id11, i) for i in pool_id2]
    id2out_calc.append(pool_id2[np.argmax(dist_id2inraw)])
    id2out_dist.append(dist_id2inraw[np.argmax(dist_id2inraw)])

df_chk = pd.DataFrame({'id1':pool_id1,'id2':id2out_calc,'id_dist':id2out_dist})
df_chk.head()

df_chk.sort_values('id_dist').head()
df_chk['id_dist'].unique().shape

df_merge2 = df_merge.merge(df_chk, left_on='BARRA_PIT_CUSIP', right_on='id1')#, how='left')
df_merge2.shape
(df_merge2['id2']==df_merge2['cusip']).sum()/df_merge2.shape[0]
dft=df_merge2[df_merge2['id2']!=df_merge2['cusip']]

