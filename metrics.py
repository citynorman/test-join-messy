# -*- coding: utf-8 -*-
"""
Created on Fri Jan 12 12:45:55 2018

@author: niemern
"""



def _jaro_winkler(ying, yang, long_tolerance=False, winklerize=True):

    ying_len = len(ying)
    yang_len = len(yang)

    if not ying_len or not yang_len:
        return 0.0

    min_len = max(ying_len, yang_len)
    searchrange = (min_len // 2) - 1
    if searchrange < 0:
        searchrange = 0

    ying_flags = [False]*ying_len
    yang_flags = [False]*yang_len

    # looking only within search range, count & flag matched pairs
    common_chars = 0
    for i, ying_ch in enumerate(ying):
        low = i - searchrange if i > searchrange else 0
        hi = i + searchrange if i + searchrange < yang_len else yang_len - 1
        for j in range(low, hi+1):
            if not yang_flags[j] and yang[j] == ying_ch:
                ying_flags[i] = yang_flags[j] = True
                common_chars += 1
                break

    # short circuit if no characters match
    if not common_chars:
        return 0.0

    # count transpositions
    k = trans_count = 0
    for i, ying_f in enumerate(ying_flags):
        if ying_f:
            for j in range(k, yang_len):
                if yang_flags[j]:
                    k = j + 1
                    break
            if ying[i] != yang[j]:
                trans_count += 1
    trans_count /= 2

    # adjust for similarities in nonmatched characters
    common_chars = float(common_chars)
    weight = ((common_chars/ying_len + common_chars/yang_len +
              (common_chars-trans_count) / common_chars)) / 3

    # winkler modification: continue to boost if strings are similar
    if winklerize and weight > 0.7 and ying_len > 3 and yang_len > 3:
        # adjust for up to first 4 chars in common
        j = min_len#min(min_len, 4)
        i = 0
        while i < j and ying[i] == yang[i] and ying[i]:
            i += 1
        if i:
            weight += i * 0.1 * (1.0 - weight)

        # optionally adjust for long strings
        # after agreeing beginning chars, at least two or more must agree and
        # agreed characters must be > half of remaining characters
        if (long_tolerance and min_len > 4 and common_chars > i+1 and
                2 * common_chars >= min_len + i):
            weight += ((1.0 - weight) * (float(common_chars-i-1) / float(ying_len+yang_len-i*2+2)))

    return weight

def levenshtein_distance(s1, s2):
    print(s1, s2)

    if s1 == s2:
        return 0
    rows = len(s1)+1
    cols = len(s2)+1
    slen =max(rows,cols)

    if not s1:
        return cols-1
    if not s2:
        return rows-1

    prev = None
    cur = range(cols)
    for r in range(1, rows):
        prev, cur = cur, [r] + [0]*(cols-1)
        for c in range(1, cols):
            score_adj = (1-c/slen)#*abs(ord(s1[r-1])-ord(s2[c-1]))/128
            deletion = prev[c] + 1*score_adj
            insertion = cur[c-1] + 1*score_adj
            edit = prev[c-1] + (0 if s1[r-1] == s2[c-1] else 1*score_adj)
            if s1[r - 1] != s2[c - 1]:
                print(s1[r-1],s2[c-1],abs(ord(s1[r-1])-ord(s2[c-1]))/128)
            cur[c] = min(edit, deletion, insertion)

    return cur[-1]

def hamming_distance(s1, s2):
    orddiffbase = ord('z')-ord('1')

    # ensure length of s1 >= s2
    if len(s2) > len(s1):
        s1, s2 = s2, s1

    # distance is difference in length + differing chars
    distance = len(s1) - len(s2)
    s2len = len(s2)
    for i, c in enumerate(s2):
        if c != s1[i]:
            distance += 1*(1-i/s2len)*abs(ord(c)-ord(s1[i]))/orddiffbase

    return distance
